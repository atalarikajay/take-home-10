import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Regis from '../src/app/Regis';
import Transaksi from '../src/app/Transaksi';
import History from '../src/app/Hisotry';
import {NavigationContainer} from '@react-navigation/native';

const Stack = createNativeStackNavigator();

const Route = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Regis" component={Regis} />
        <Stack.Screen name="Transaksi" component={Transaksi} />
        <Stack.Screen name="History" component={History} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Route;
