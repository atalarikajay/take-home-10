import {View, Text} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {Getone} from '../src/redux/action/Action';

const Home = () => {
  const [nama, setNama] = useState('');
  // cara kirim data ke action
  const dispatch = useDispatch();
  const getAction = async () => {
    try {
      dispatch(Getone());
    } catch (e) {
      console.log(e, 'ini');
    }
  };

  useEffect(() => {
    getAction();
  }, []);

  // cara ambil data dari reducer adalah
  const state = useSelector(state => state.Fetchs);
  console.log('ini auth lhoo', state);

  return (
    <View>
      <Text>home</Text>
    </View>
  );
};

export default Home;
