import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  Image,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {getHist} from '../redux/action/Action';
import {useDispatch, useSelector} from 'react-redux';

const History = () => {
  const [datas, setDatas] = useState(null);
  const dispatch = useDispatch();
  const Histori = async () => {
    try {
      dispatch(getHist());
    } catch (e) {
      console.log(e);
    }
  };
  const states = useSelector(state => state.Fetchs?.History);
  const hasil = Object.entries(states);

  useEffect(() => {
    Histori();
  }, []);

  return (
    <SafeAreaView style={style.container}>
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <View
          style={{
            backgroundColor: 'green',
            height: 50,
          }}
        />
        <View
          style={{
            marginBottom: -30,
            justifyContent: 'center',
          }}></View>
        <View style={style.headerBar}>
          <Text style={style.txtBar}>History</Text>
        </View>
        <ScrollView contentContainerStyle={{padding: 20}}>
          <View>
            {hasil?.map((e, index) => {
              return (
                <View
                  style={{
                    backgroundColor: 'white',
                    width: '100%',
                    height: 110,
                    elevation: 5,
                    shadowColor: 'black',
                    shadowOpacity: 0.2,
                    shadowRadius: 4,
                    shadowOffset: {
                      height: 1,
                      width: 2,
                    },
                    paddingVertical: 15,
                    marginBottom: 15,
                    borderRadius: 10,
                  }}>
                  <View>
                    <View style={{marginLeft: 15, marginTop: 5}}>
                      <Text style={{fontWeight: 'bold'}}>
                        Sender : {e[1].sender}
                      </Text>
                      <Text style={{marginTop: 10}}>
                        Jumlah : Rp. {e[1].amount}
                      </Text>
                      <Text
                        style={{
                          fontFamily: 'ShantellSans-Bold',
                          marginTop: 10,
                        }}>
                        Metode : {e[1].type}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                      <View>
                        <Image
                          source={{
                            uri: 'https://cdn-icons-png.flaticon.com/512/2037/2037881.png',
                          }}
                          style={{
                            width: 45,
                            height: 45,
                            marginLeft: 295,
                            marginTop: -50,
                          }}
                        />
                      </View>
                    </View>
                  </View>
                </View>
              );
            })}
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'green',
    overflow: 'hidden',
    justifyContent: 'center',
  },
  headerBar: {
    backgroundColor: '#F0F8FF',
    height: 50,
    width: '70%',
    elevation: 5,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 60,
  },
  txtBar: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
export default History;
