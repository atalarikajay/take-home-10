import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import React, {useState} from 'react';
import axios from 'axios';
import {sendRegis} from '../redux/action/Action';
import {useDispatch, useSelector} from 'react-redux';

const Regis = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const dispatch = useDispatch();
  async function Register() {
    try {
      dispatch(sendRegis(username, email, password));
      // navigation.navigate('Home');
    } catch (e) {
      console.log('errors', e);
    }
  }

  const state = useSelector(state => state.Auth);
  console.log('ini auth lhoo', state);
  return (
    <SafeAreaView style={style.container}>
      <View style={style.head}>
        <View style={style.vBack}>
          <Image
            style={style.back}
            source={{
              uri: 'https://cdn-icons-png.flaticon.com/512/93/93634.png',
            }}
          />
        </View>
        <View style={style.vGambar}>
          <Image
            style={style.gambar}
            source={{
              uri: 'https://2.bp.blogspot.com/-WX31NAPA5Bw/W8DasjMJH0I/AAAAAAAAEQo/ORaHd-6H24s_6E0V4unaDIQUD5V2-D-5gCLcBGAs/s1600/bank%2Basia%2Blimited%2Bcomilla%2Bbranch.jpg',
            }}
          />
        </View>
        <View style={style.txtUsername}>
          <TextInput
            placeholder="Masukkan Username Anda"
            placeholderTextColor={'rgba(0,0,0,0.5)'}
            onChangeText={send => {
              setUsername(send);
            }}
          />
        </View>
        <View style={style.txtPass}>
          <TextInput
            placeholder="Masukkan Email Anda"
            placeholderTextColor={'rgba(0,0,0,0.5)'}
            onChangeText={send => {
              setEmail(send);
            }}
          />
        </View>
        <View style={style.txtPass}>
          <TextInput
            placeholder="Masukkan Password Anda"
            placeholderTextColor={'rgba(0,0,0,0.5)'}
            secureTextEntry={true}
            onChangeText={send => {
              setPassword(send);
            }}
          />
        </View>
        <View style={style.vTochsubmit}>
          <TouchableOpacity
            style={style.tochSubmit}
            onPress={() => {
              Register(username, email, password);
              navigation.navigate('Transaksi');
            }}>
            <Text style={style.txtSubmit}>SUBMIT</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
  },
  head: {
    paddingHorizontal: 50,
  },
  vBack: {
    marginTop: Platform.OS == 'ios' ? null : 8,
    marginLeft: -40,
    width: 60,
  },
  back: {
    width: 40,
    height: 40,
  },
  vGambar: {
    marginTop: 15,
    flexDirection: 'row',
    elevation: 5,
    borderRadius: 8,
    shadowColor: 'black',
    shadowOpacity: 0.3,
    shadowRadius: 0.3,
    shadowOffset: {
      height: 1.5,
      width: 1.5,
    },
  },
  gambar: {
    resizeMode: 'contain',
    width: '100%',
    height: 110,
  },
  txtUsername: {
    height: Platform.OS == 'ios' ? 50 : 42,
    justifyContent: 'center',
    marginTop: Platform.OS == 'ios' ? 55 : 42,
    borderWidth: 0.3,
    borderRadius: 8,
    borderColor: 'gray',
    paddingHorizontal: 8,
  },
  txtPass: {
    height: Platform.OS == 'ios' ? 50 : 42,
    justifyContent: 'center',
    marginTop: 20,
    borderWidth: 0.3,
    borderRadius: 8,
    borderColor: 'gray',
    paddingHorizontal: 8,
  },
  vTochsubmit: {
    marginTop: Platform.OS == 'ios' ? 35 : 25,
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tochSubmit: {
    width: 150,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 80,
    borderRadius: 8,
    marginHorizontal: Platform.OS == 'ios' ? 47 : 59,
    backgroundColor: '#62a7c8',
    elevation: 5,
    shadowColor: 'gray',
    shadowOpacity: 0.6,
    shadowRadius: 0.1,
    shadowOffset: {
      height: 1,
      width: 1,
    },
  },
  txtSubmit: {
    fontWeight: 'bold',
    fontSize: 18,
    color: 'white',
  },
});

export default Regis;
