import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  Platform,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import {TextInput} from 'react-native';
import Modal from 'react-native-modal';
import axios from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import {sendPay} from '../redux/action/Action';

const Transaksi = ({navigation}) => {
  const [dataAmount, setDataAmount] = useState('');
  const [dataSender, setDataSender] = useState('');
  const [dataTarget, setDataTarget] = useState('');
  const [dataType, setDataType] = useState('');
  const [modal, setModal] = useState(false);
  const [status, setStatus] = useState('');

  const dispatch = useDispatch();
  async function Payment() {
    try {
      dispatch(sendPay(dataAmount, dataSender, dataTarget, dataType));
      setStatus('Transaksi berhasil!!');
      handleModal();
    } catch (e) {
      console.log('errors', e);
      setStatus('Maaf, Transaksi anda Gagal');
    }
  }

  const handleModal = () => {
    setModal(true);
  };
  const state = useSelector(state => state.Fetchs?.Transaksi);
  console.log('ini auth lhoo', state);
  return (
    <SafeAreaView style={style.container}>
      <View
        style={{
          height: Platform.OS == 'ios' ? '110%' : '100%',
          backgroundColor: 'white',
        }}>
        <View
          style={{
            backgroundColor: 'green',
            height: 60,
          }}
        />
        <View
          style={{
            marginBottom: -30,
            justifyContent: 'center',
          }}></View>
        <View style={style.headerBar}>
          <Text style={style.txtBar}>Payment</Text>
        </View>
        <View style={style.body}>
          <View style={style.ltrInput}>
            <View style={{flexDirection: 'row'}}>
              <Text style={style.txtSJ}>Sender </Text>
              <View style={style.txtInput}>
                <TextInput
                  placeholder="  Masukkan Nama Pengirim"
                  value={dataSender}
                  onChangeText={send => {
                    setDataSender(send);
                  }}
                />
              </View>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={style.txtPR}>Payment </Text>
              <View style={style.txtInput}>
                <TextInput
                  placeholder="  Tentukan Jenis Payment"
                  value={dataType}
                  onChangeText={send => {
                    setDataType(send);
                  }}
                />
              </View>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={style.txtSJ}>Jumlah</Text>
              <View style={style.txtInput}>
                <TextInput
                  placeholder="  Masukkan Jumlah Uang"
                  value={dataAmount}
                  onChangeText={send => {
                    setDataAmount(send);
                  }}
                />
              </View>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={style.txtPR}>Receiver</Text>
              <View style={style.txtInput}>
                <TextInput
                  placeholder="  Masukkan Nama Penerima"
                  value={dataTarget}
                  onChangeText={send => {
                    setDataTarget(send);
                  }}
                />
              </View>
            </View>
            <View style={style.vToch}>
              <TouchableOpacity
                style={style.tochKirim}
                onPress={() => {
                  Payment();
                }}>
                <Text>Send</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <Modal isVisible={modal} animationOutTiming={1000}>
          <View
            style={{
              backgroundColor: 'white',
              paddingVertical: 13,
              width: 250,
              borderRadius: 8,
              marginLeft: 60,
            }}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image
                source={{
                  uri: 'https://ipaymu.com/wp-content/uploads/2020/02/instalment-icon-05.png',
                }}
                style={{width: 100, height: 100}}
              />
              <Text
                style={{
                  fontWeight: 'bold',
                  marginTop: 15,
                  fontSize: 18,
                  color: 'black',
                }}>
                {status}
              </Text>
              <Text>Sender: {dataSender}</Text>
              <Text>Receiver: {dataTarget}</Text>
              <Text>Jumlah : Rp. {dataAmount}</Text>
              <Text>Payment : {dataType}</Text>
            </View>
            <View
              style={{
                marginTop: 20,
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity
                style={{
                  backgroundColor: 'green',
                  paddingHorizontal: 40,
                  paddingVertical: 18,
                  borderWidth: 1,
                  borderColor: 'green',
                  borderRadius: 8,
                }}
                onPress={() => navigation.navigate('History')}>
                <Text>OK</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    </SafeAreaView>
  );
};
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'green',
    overflow: 'hidden',
    justifyContent: 'center',
  },
  headerBar: {
    backgroundColor: '#F0F8FF',
    height: 50,
    width: '70%',
    elevation: 5,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 60,
  },
  txtBar: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
  },
  body: {
    height: 350,
    width: 350,
    marginTop: 100,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 30,
  },
  ltrInput: {
    width: 350,
    height: 400,
    alignItems: 'center',
    marginRight: 15,
  },
  txtInput: {
    height: Platform.OS == 'ios' ? 45 : null,
    width: 250,
    borderColor: 'gray',
    borderWidth: 0.5,
    borderRadius: 8,
    marginVertical: 15,
    justifyContent: 'center',
    marginRight: 20,
  },
  txtSJ: {
    fontWeight: 'bold',
    marginHorizontal: 15,
    marginTop: 28,
    marginRight: 25,
  },
  txtPR: {
    fontWeight: 'bold',
    marginHorizontal: 15,
    marginTop: 28,
  },
  vToch: {
    marginLeft: 30,
  },
  tochKirim: {
    marginTop: 25,
    width: 170,
    height: 60,
    backgroundColor: '#7CFC00',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    borderColor: 'gray',
    borderWidth: 0.3,
    elevation: 5,
    shadowOpacity: 0.5,
    shadowRadius: 0.2,
    shadowOffset: {
      height: 0.5,
      width: 0.5,
    },
  },
});
export default Transaksi;
