import React, {useState} from 'react';
import {
  View,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  Text,
  Platform,
  StyleSheet,
  KeyboardAvoidingView,
} from 'react-native';

const Async = ({onSave}) => {
  const [Nama, setNama] = useState('');
  const [Umur, setUmur] = useState('');
  const [Email, setEmail] = useState('');
  const [NomorHP, setHP] = useState('');
  const [Alamat, setAlamat] = useState('');

  const handleSave = () => {
    onSave({Nama, Umur, Email, NomorHP, Alamat});
  };

  return (
    <KeyboardAvoidingView>
      <SafeAreaView style={style.container}>
        <View style={style.txtInama}>
          <TextInput
            style={style.textnama}
            placeholder="Masukkan Nama Anda"
            value={Nama}
            onChangeText={setNama}
            placeholderTextColor="black"
          />

          <TextInput
            style={style.textumur}
            placeholder="Berapa Umur Anda ?"
            value={Umur}
            onChangeText={setUmur}
            placeholderTextColor="black"
          />
          <TextInput
            style={style.textemail}
            placeholder="Masukkan Email Anda"
            value={Email}
            onChangeText={setEmail}
            placeholderTextColor="black"
          />
          <TextInput
            style={style.textnohp}
            placeholder="Masukkan Nomor Handphone Anda"
            value={NomorHP}
            onChangeText={setHP}
            placeholderTextColor="black"
          />
          <TextInput
            style={style.textalamat}
            placeholder="Dimana Alamat Anda ?"
            value={Alamat}
            onChangeText={setAlamat}
            placeholderTextColor="black"
          />
          <TouchableOpacity style={style.submit} onPress={handleSave}>
            <Text style={style.textsave}>Submit</Text>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const style = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: 420,
    height: 1000,
  },
  txtInama: {
    width: Platform.OS == 'ios' ? 380 : 390,
    height: Platform.OS == 'ios' ? 460 : 480,
    marginTop: Platform.OS == 'ios' ? 135 : 90,
    borderRadius: 20,
    borderWidth: 1,
    marginHorizontal: Platform.OS == 'ios' ? 6 : 10,
  },
  textnama: {
    fontSize: 14,
    paddingVertical: 15,
    borderWidth: 1,
    width: 300,
    marginLeft: 45,
    marginTop: Platform.OS == 'ios' ? 23 : 16,
    borderRadius: 8,
  },
  textumur: {
    fontSize: 14,
    paddingVertical: 15,
    borderWidth: 1,
    width: 300,
    marginLeft: 45,
    marginTop: Platform.OS == 'ios' ? 20 : 16,
    borderRadius: 8,
  },
  textemail: {
    fontSize: 14,
    paddingVertical: 15,
    borderWidth: 1,
    width: 300,
    marginLeft: 45,
    marginTop: Platform.OS == 'ios' ? 20 : 16,
    borderRadius: 8,
  },
  textnohp: {
    fontSize: 14,
    paddingVertical: 15,
    borderWidth: 1,
    width: 300,
    marginLeft: 45,
    marginTop: Platform.OS == 'ios' ? 20 : 16,
    borderRadius: 8,
  },
  textalamat: {
    fontSize: 14,
    paddingVertical: 15,
    borderWidth: 1,
    width: 300,
    marginLeft: 45,
    marginTop: Platform.OS == 'ios' ? 20 : 16,
    borderRadius: 8,
  },
  submit: {
    backgroundColor: 'gray',
    borderRadius: 8,
    width: 180,
    height: 40,
    marginTop: Platform.OS == 'ios' ? 30 : 25,
    marginLeft: Platform.OS == 'ios' ? 105 : 110,
    borderWidth: 0.5,
    borderColor: 'gray',
  },
  textsave: {
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    top: 6,
  },
});

export default Async;
