import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  StyleSheet,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Async from './Async';

const DataUser = () => {
  const [identitas, setIdentitas] = useState(null);
  const [showInput, setShowInput] = useState(false);

  const getProfile = async () => {
    const jsonValue = await AsyncStorage.getItem('@profile');
    setIdentitas(jsonValue != null ? JSON.parse(jsonValue) : null);
  };

  const saveProfile = async newProfile => {
    await AsyncStorage.setItem('@profile', JSON.stringify(newProfile));
    setIdentitas(newProfile);
    setShowInput(false);
  };

  useEffect(() => {
    getProfile();
  }, []);

  return (
    <SafeAreaView style={style.container}>
      <View>
        {showInput ? (
          <Async onSave={saveProfile} />
        ) : (
          <View>
            {identitas ? (
              <View style={style.box}>
                <View style={style.vText}>
                  <Text style={style.txtnama}>Nama</Text>
                  <Text style={style.textnama}>{identitas.Nama}</Text>
                </View>
                <View style={style.vText}>
                  <Text style={style.txtumur}>Umur</Text>
                  <Text style={style.textumur}>{identitas.Umur}</Text>
                </View>
                <View style={style.vText}>
                  <Text style={style.txtemail}>Email</Text>
                  <Text style={style.textemail}>{identitas.Email}</Text>
                </View>
                <View style={style.vText}>
                  <Text style={style.txtnohp}>Nomor Hp</Text>
                  <Text style={style.textnohp}>{identitas.NomorHP}</Text>
                </View>
                <View style={style.vText}>
                  <Text style={style.txtalamat}>Alamat</Text>
                  <Text style={style.textalamat}>{identitas.Alamat}</Text>
                </View>
                <TouchableOpacity
                  style={style.tochDatadiri}
                  onPress={() => setShowInput(true)}>
                  <Text style={style.textcreateacc}>Buat Data Diri</Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={style.vToch}>
                <TouchableOpacity
                  onPress={() => setShowInput(true)}
                  style={style.tochTambah}>
                  <Text style={style.txtTmbh}>Tambah Data</Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  container: {backgroundColor: 'white', width: 420, height: 1000},
  box: {
    width: Platform.OS == 'ios' ? 370 : 390,
    height: Platform.OS == 'ios' ? 430 : 470,
    marginTop: Platform.OS == 'ios' ? 120 : 70,
    borderRadius: 20,
    marginHorizontal: Platform.OS == 'ios' ? 12 : 11,
    borderWidth: 1,
  },
  txtnama: {
    marginTop: 15,
    marginLeft: 5,
  },
  txtumur: {
    marginTop: Platform.OS == 'ios' ? 17 : 25,
    marginLeft: 5,
  },
  txtemail: {
    marginTop: Platform.OS == 'ios' ? 17 : 25,
    marginLeft: 5,
  },
  txtnohp: {
    marginTop: Platform.OS == 'ios' ? 17 : 25,
    marginLeft: 5,
  },
  txtalamat: {
    marginTop: Platform.OS == 'ios' ? 17 : 25,
    marginLeft: 5,
  },
  textnama: {
    fontSize: 14,
    paddingVertical: 15,
    borderWidth: 1,
    width: Platform.OS == 'ios' ? 270 : 300,
    marginLeft: Platform.OS == 'ios' ? 45 : 40,
    borderRadius: 8,
  },
  textumur: {
    fontSize: 14,
    paddingVertical: 15,
    borderWidth: 1,
    width: Platform.OS == 'ios' ? 270 : 300,
    marginLeft: Platform.OS == 'ios' ? 45 : 43,
    marginTop: Platform.OS == 'ios' ? 5 : 10,
    borderRadius: 8,
  },
  textemail: {
    fontSize: 14,
    paddingVertical: 15,
    borderWidth: 1,
    width: Platform.OS == 'ios' ? 270 : 300,
    marginLeft: Platform.OS == 'ios' ? 45 : 41,
    marginTop: Platform.OS == 'ios' ? 5 : 10,
    borderRadius: 8,
  },
  textnohp: {
    fontSize: 14,
    paddingVertical: 15,
    borderWidth: 1,
    width: Platform.OS == 'ios' ? 270 : 300,
    marginLeft: Platform.OS == 'ios' ? 12 : 11,
    marginTop: Platform.OS == 'ios' ? 5 : 10,
    borderRadius: 8,
  },
  textalamat: {
    fontSize: 14,
    paddingVertical: 15,
    borderWidth: 1,
    width: Platform.OS == 'ios' ? 270 : 300,
    marginLeft: Platform.OS == 'ios' ? 31 : 30,
    marginTop: Platform.OS == 'ios' ? 5 : 10,
    borderRadius: 8,
  },
  vText: {
    flexDirection: 'row',
    marginTop: 25,
  },
  vToch: {
    alignItems: 'center',
    marginTop: Platform.OS == 'ios' ? 350 : 280,
    marginRight: Platform.OS == 'ios' ? 20 : null,
  },
  tochTambah: {
    backgroundColor: 'cyan',
    paddingVertical: 17,
    width: 200,
    alignItems: 'center',
    borderColor: 'gray',
    borderRadius: 8,
    borderWidth: 0.2,
  },
  txtTmbh: {
    fontWeight: 'bold',
    fontSize: 12,
  },
  tochDatadiri: {
    backgroundColor: 'red',
    borderRadius: 20,
    width: 180,
    height: 35,
    overflow: 'hidden',
    marginTop: Platform.OS == 'ios' ? 70 : 60,
    marginLeft: Platform.OS == 'ios' ? 100 : 115,
  },
  textcreateacc: {
    color: 'black',
    fontSize: 16,
    textAlign: 'center',
    top: 6,
  },
});
export default DataUser;
