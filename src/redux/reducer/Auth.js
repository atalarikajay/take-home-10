const initialState = {
  DataAuth: null, //null merupakan tipe data, digunakan sesuai dengan kebutuhan
};

const Auth = (state = initialState, action) => {
  switch (
    action.type // kenapa .type karena tipe apa saja yang dibutuhkan
  ) {
    case 'REGIS':
      return {
        ...state,
        DataAuth: action.data,
      };
    default:
      return state;
  }
};

export default Auth;
