import {combineReducers} from 'redux';
import Auth from './Auth';
import Fetchs from './Fetchs';

export default combineReducers({
  Auth,
  Fetchs,
});
