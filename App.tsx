import {View, Text} from 'react-native';
import React from 'react';
import {Provider} from 'react-redux';
import Route from './route/Route';
import storeRedux from './src/redux/Store';

const App = () => {
  return (
    <Provider store={storeRedux}>
      <Route />
    </Provider>
  );
};

export default App;
